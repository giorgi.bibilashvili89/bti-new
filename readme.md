# Project Description

## Instalation

### Clone the repository

Clone this repository and checkout to laravel branch. 
then copy .env.example to .env setup and your own config

### Virtual Hosts

Setup apache virtual host to /public directory

```apacheconfig
<VirtualHost *:80>
    DocumentRoot "{PATH}\vod\public"
    ServerName vod.local
    ServerAlias www.vod.local
    <Directory "{PATH}\vod\public">
        Order allow,deny
        Allow from all
    </Directory>
</VirtualHost>
```

### Install PHP dependencies

Run command bellow:

```shell
composer install
```

### Database migrations

Run this magic command to create database schema and default data :)

```shell
php artisan migrate:refresh --seed
```


## Who
The client is [BTI Studio](http://www.btistudios.com/), an international dubbing and subtitling company.

## Problem Description
The actual validation process is extremly slow and painfull.
The client receives a video, he needs to give his feedback via email then BTI reworks on the video, sends it again to client etc...

They actually use WeTransfer (yea, I know...) to do that and that's unsecure and unefficient.

## Solution
We built a simple one page prototype using VueJs to show them how an inline validation tool could work.
It led to some dicussion and more prototyping and in the end they asked for a full product management solution.
I shared with you the Drive Folder with the mockups for Desktop and mobile version.

The latest files are in DESK/RETOURS BTI, that's what you should focus on, you can take a look at the MOBILE version but keep in mind that's not 100% validated.

## Current state 
I shared with you the bitbucket repository, we have two branches here:
- Master, first prototype, poorly coded, you should be able to get the code for the video page from here
- Master-object, the begining of a rewrite in OO Php, early stage rewrite. Benjamin started this and was needed on another project.

If you want to start a third branch and pick pieces from the other two that's ok with me. Or if you think you can continue working on the master-object rerite branch that's also fine.


# Project Architecture

## Hardware
This project will need to run internally in BTI's office on a QNAP NAS so we are limited to PHP 5.5

## Details
In this project we have two types of account

- BTI accounts
- Client accounts

BTI accounts are "admin" accounts, they can publish videos for the client to see, validate clients accounts, manage which client can access which product.
BTI accounts will be created manually in the admin panel.

Clients accounts are created on the tool, clients will need to be validated and attaches to a BTI account in the admin panel. We don't have mockups for that at the moment.

## Mockups
[See All mockups Here](https://drive.google.com/drive/u/0/folders/1LCzfqpe3Wvz0MVffZ25VFJDWdh6nTK0i)

### 00.jpg
Here you can login or sign up.
If you already have an account, you enter your email address and your [Google Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en) code.
They don't want to have to remeber a password so we opted for that solution, let me know if you see anything better.
I never user this but it looks like there's a bunch of PHP libraries.

I found this [article](https://medium.com/@richb_/easy-two-factor-authentication-2fa-with-google-authenticator-php-108388a1ea23) and they use this [library](https://packagist.org/packages/sonata-project/google-authenticator)

It's ok if you need to change the page design to add / remove elements for the login or in general, feel free to do what you think will work the best.

The "Create an account" link takes you to a page were you can create your account, I think that's already done, you can find it in the actual code.
The account created here will need to be validated by a BTI account in the admin panel, an email alert also needs to be sent.

### 01.jpg
This is the [modal](https://getbootstrap.com/docs/3.3/javascript/#modals) were you can enter yout Google Authenticator code in order to connect. You can change the text because the code will not be sent via SMS.

### 10.jpg
This is the CLIENT view of the dashboard.
The client lands on the "Recently added" tab.

Recently added video are videos with no comments.

### 11.jpg
Client view of a recent video without comments.

This page uses [VideoJs](https://github.com/videojs/video.js) I think the major part of functionalities for that page are done.

You can switch between the Original Version (OV) and the Translated Version (FR).

The client can add new comments each comment is linked to the video timestamp and if you click on a comment it takes you back to that timestamp in the video.

Comments can be edited / deleted.

The "Download Script" link download a PDF file that can be attached to the video when the admin makes the video available.

### 12.jpg
Modal showing the list of the contacts actually linked to that product

### 20.jpg
Pending tab.

A video is considered pending as soon as it have a comment.

### 21.jpg
Video view of a pending video with comments.

The client can also validate the video, in this case a confiration modal appears.

### 22.jpg
Confirmation modal

### 23.jpg
Validation confirmation

### 24.jpg
Video view of a BTI account.

As you can see BTI accounts will have access to a "Back Office" tab.

BTI accounts can only answer comments, they can't create new ones. The rest of the page stays the same.

### 30.jpg
History tab with validated videos.

### 31.jpg
You can search by month

### 32.jpg
Another view of the history tab (not sure what the designer was trying to show here)

### 33.jpg
Video view of an old video.

In this case the video file is not available anymore so me need to show a message.
Also, all comments are read-only because the video as been validated.

### Back Office
We don't have mockups yet but we should have them soon.
BTI accounts need to be able to do a few things in that tab:
- Validate Client accounts
- Link clients accounts to one or more BTI accounts
- Link BTI accounts to products (TV show or Documentary etc...)
- Select videos available to the clients (and add the script in pdf)
- When releasing a video, the BTI account needs to add the starting timestamp of the video so we can synchronize the comments timestamp to the video incrusted timestamp (let me know if that makes sense, this should already be done on the video page)

All the videos are stored on a local server connected to the NAS (or maybe even on the NAS) so we need to write a script that will browser the folders and insert all the videos in the DB.

We need to determine the file name format for the script to be able to parse name and language of the video, I'll give that to you ASAP.
The file structure should be something like this
```
--- Game_of_Thrones
------- season1
----------- episode1_fv_valid.avi
----------- episode1_ov_valid.avi
----------- episode2_fv.avi
------- season2
```

In this example only the _valid files will be indexed by the script and added to the database.

Then episode1 will show up only the back office tab, it will need to be released by a BTI account to be showed in the "Recently Added" tab on the client account.
