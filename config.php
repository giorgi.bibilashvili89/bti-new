<?php
// Environement var settings




if (strpos($_SERVER["HTTP_HOST"], "localhost") !== false) {
    error_reporting(E_ALL & ~E_NOTICE);
    ini_set("display_errors", 1);
    define("ENV_DEV", true);
    define("ENV_LOCAL", true);
    $config["db"]["host"]	    = "127.0.0.1";
    $config["db"]["login"]		= "root";
    $config["db"]["password"]	= "root";
    $config["db"]["name"]		= "post-prod";
    $config["db"]["dsn"]		= "mysql:host=".$config["db"]["host"].";dbname=".$config["db"]["name"];
    define("ROOT_PATH", $_SERVER["DOCUMENT_ROOT"]);
    define("JS_VERSION", time());
    define("CSS_VERSION", time());
} elseif (strpos($_SERVER["HTTP_HOST"], "vod.liwa.fr") !== false) {
    error_reporting(E_ALL & ~E_NOTICE);
    ini_set("display_errors", 1);
    define("ENV_DEV", true);
    define("ENV_LOCAL", false);
    $config["db"]["host"]	    = "rj13754-001.privatesql";
    $config["db"]["login"]		= "vod";
    $config["db"]["password"]	= "bMv939bHunjT";
    $config["db"]["name"]		= "vod";
    $config["db"]["dsn"]        = "mysql:host=".$config["db"]["host"].";dbname=".$config["db"]["name"].";port=35455";


    define("ROOT_PATH", $_SERVER["DOCUMENT_ROOT"] . "/");
    define("JS_VERSION", "1801150");
    define("CSS_VERSION", "1801150");
}elseif (strpos($_SERVER["HTTP_HOST"], "vod.local") !== false) {
    error_reporting(E_ALL & ~E_NOTICE);
    ini_set("display_errors", 1);
    define("ENV_DEV", true);
    define("ENV_LOCAL", true);
    $config["db"]["host"]	    = "localhost";
    $config["db"]["login"]		= "root";
    $config["db"]["password"]	= "";
    $config["db"]["name"]		= "vod";
    $config["db"]["dsn"]		= "mysql:host=".$config["db"]["host"].";dbname=".$config["db"]["name"];
    define("ROOT_PATH", $_SERVER["DOCUMENT_ROOT"]);
    define("JS_VERSION", time());
    define("CSS_VERSION", time());
}
else {
    define("ENV_DEV", false);
}
?>