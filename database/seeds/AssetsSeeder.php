<?php

use Illuminate\Database\Seeder;
use App\Models\Assets;

class AssetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Assets::create([
            'video_id' => 1,
            'name'     => 'sample',
            'path'     => 'sample.pdf',
        ]);

        \App\Models\Assets::updateOrCreate(
            ['id' => 1],
            [
                'name'     => 'sample',
                'path'     => 'sample.pdf',
            ]
        );
    }
}
