<?php

Route::post('/2fa', function () {
    return redirect(URL()->previous());
})->name('2fa')->middleware('2fa');

Route::get('2fa', function () {
   return redirect('home');
});

Route::get('google2fa', 'Auth\LoginController@google2fa')->name('google2fa');

Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

Route::get('success/register', 'Auth\RegisterController@showSuccessRegister')->name('success.register');

Route::group([
    'middleware' => ['2fa', 'auth']
], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/series/{id}', 'SeriesController@index')->name('series');
    Route::get('/video/{id}', 'VideoController@index')->name('video');
    Route::post('/approvo/video', 'VideoController@approvo')->name('approvo.video');

    Route::post('/add/comment', 'VideoController@addComment')->name('add.comment');
    Route::put('/edit/comment', 'VideoController@editComment')->name('edit.comment');
    Route::delete('/delete/comment', 'VideoController@deleteComment')->name('delete.comment');
});

Route::group([
    'prefix'    => config('admin.admin_url'),
    'as'        => 'admin.',
    'namespace' => 'Admin\Auth'
], function () {
    Route::get('/login', 'LoginController@showLoginForm')->name('show.login')->middleware('admin.login');
    Route::post('/login', 'LoginController@login')->name('login');
    Route::post('/logout', 'LoginController@logout')->name('logout');
});

Route::group([
    'middleware' => ['admin'],
    'prefix'     => config('admin.admin_url'),
    'as'         => 'admin.',
    'namespace'  => 'Admin'
], function () {
    Route::get('/', 'UserRequestController@index')->name('dashboard');
    Route::post('change/user/status', 'UserRequestController@changeUserStatus')->name('change.user.status');

    Route::get('users', 'UsersController@index')->name('users');

    Route::put('user/change/permission', 'UsersController@changePermission')->name('user.change.permission');
    Route::put('user/change/active/status', 'UsersController@changeActiveStatus')->name('user.change.active.status');
    Route::delete('user/delete', 'UsersController@deleteUser')->name('user.delete');
    Route::put('edit/user', 'UsersController@editUser')->name('edit.user');

    Route::get('get/team/list', 'TeamsController@getTeamList')->name('get.team.list');
    Route::post('add/team/user', 'TeamsController@addUser')->name('add.team.user');
    Route::post('add/team', 'TeamsController@addTeam')->name('add.team');
    Route::put('edit/team', 'TeamsController@editTeam')->name('add.team');
    Route::delete('delete/team/user', 'TeamsController@DeleteUser')->name('delete.team.user');
    Route::delete('delete/team', 'TeamsController@deleteTeam')->name('delete.team');

    Route::get('tree', 'TreeController@index')->name('tree');
    Route::post('video/group', 'TreeController@videoGroup')->name('video.group');
});