<?php

return [
    1000 => [
        'status'  => false,
        'message' => 'Something went wrong',
    ],
    1001 => [
        'status'  => false,
        'message' => 'This user request don`\t exist',
    ],
    1002 => [
        'status'  => false,
        'message' => 'This user is not validated',
    ],
    1003 => [
        'status'  => false,
        'message' => 'This team in not validated',
    ],
    1004 => [
        'status'  => false,
        'message' => 'This user is already in that group',
    ],
    1005 => [
        'status'  => false,
        'message' => 'This group already exist',
    ]
];