/**
 * Video Time Format, Change Second To TIme Format (E.x 10 Second = 00:00:10:00)
 *
 * @param second
 * @returns {string}
 */
export const videoTimeFormat = (second) => {

    let pad = (second) => (second < 10) ? '0' + second : second

    return [
        pad(Math.floor(second / 3600)),
        pad(Math.floor(second % 3600 / 60)),
        pad(Math.floor(second % 60))
    ].join(':')
}