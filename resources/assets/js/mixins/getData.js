import axios from 'axios'

export default {
    data() {
        return {
            loading: false
        }
    },
    methods: {
        async asyncGetData(request) {
            // Start Loading
            this.loading = true
            // Send Async Request
            return await axios({
                method: request.method,
                url: '/' + request.url,
                data: request.data
            })
                .then(response => {
                    // Stop Loading
                    this.loading = false
                    // Return Error
                    return response.data
                })
                .catch(error => {
                    // Stop Loading
                    this.loading = false
                    // Set Response Error Alert
                    this.$message.error(error.response.data.message)
                    // Return Error Response
                    return error.response.data
                })
        },
        getData(request) {
            // Start Loading
            this.loading = true
            // Send Request
            return axios({
                method: request.method,
                url: '/' + request.url,
                data: request.data
            }).then(response => {this.loading = false})
                .catch(error => {
                    console.log(error)
                    // Stop Loading
                    this.loading = false
                    // Set Response Error Alert
                    this.$message.error(error.response.data.message)
                })
        },
    }
}