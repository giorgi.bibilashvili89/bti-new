
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');

import store from './store'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('user-request', require('./components/UserRequest').default);
Vue.component('users', require('./components/Users').default);
Vue.component('tree', require('./components/Tree').default);
Vue.component('video-page', require('./components/Video/VideoPage').default);

const app = new Vue({
    el: '#app',
    store
});
