@extends('layouts.main-layout')

@section('content')
    <form class="header-search" action="">
        <input type="text" class="header-search__input" name="search"
               value="{{ isset($request->search) ? $request->search : '' }}"/>
        <button class="header-search__button">
            <i class="fa fa-search" aria-hidden="true"></i>
        </button>
    </form>

    <div class="main-container">
        <div class="sort">
            <form action="{{ route('home') }}" method="GET">
                <select name="sort" onchange="this.form.submit()" class="sort__select">
                    <option value="0" @if($request->sort == 0) selected @endif>data (recent)</option>
                    <option value="1" @if($request->sort == 1) selected @endif>data (older)</option>
                    <option value="2" @if($request->sort == 2) selected @endif>name (A > Z)</option>
                    <option value="3" @if($request->sort == 3) selected @endif>name (Z > A)</option>
                </select>
            </form>
        </div>

        <div class="home-list">
            <div class="home-list__title">
                <i class="fa fa-youtube-play home-list__icon" aria-hidden="true"></i>
                <div>
                    <h3 class="home-list__main-title-text">newly added</h3>
                    <span class="home-list__title-text">Files that have been recently added by BTI team, that you haven't reviewed yet.</span>
                </div>
            </div>

            <div class="home-list__items">
                @foreach($response['new'] as $series)
                    <a class="home-list__item" href="{{ route('series', [$series['info']->id]) }}">
                        <div class="home-list__left-item">
                            <div class="home-list__item-icon">
                                <i class="fa fa-folder-o" aria-hidden="true"></i>
                            </div>

                            <div class="home-list__item-text">
                                <h4>{{ $series['info']->name }}</h4>
                                <span>type: DUBBING</span>
                            </div>
                        </div>

                        <div class="home-list__right-item">
                            <div class="home-list__item-text">
                                <h4>{{ $series['video_length'] }} new files</h4>
                                <span>Added {{ $series['date'] }}</span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>

        <div class="home-list">
            <div class="home-list__title">
                <i class="fa fa-youtube-play home-list__icon" aria-hidden="true"></i>
                <div>
                    <h3 class="home-list__main-title-text">review in progress</h3>
                    <span class="home-list__title-text">Files that you have already started to review, but not approved yet.</span>
                </div>
            </div>

            <div class="home-list__items">
                @foreach($response['reviewed'] as $series)
                    <a class="home-list__item" href="{{ route('series', [$series['info']->id]) }}">
                        <div class="home-list__left-item">
                            <div class="home-list__item-icon">
                                <i class="fa fa-folder-o" aria-hidden="true"></i>
                            </div>

                            <div class="home-list__item-text">
                                <h4>{{ $series['info']->name }}</h4>
                                <span>type: DUBBING</span>
                            </div>
                        </div>

                        <div class="home-list__right-item">
                            <div class="home-list__item-text">
                                <h4>{{ $series['video_length'] }} files
                                    ({{ $response['new'][$series['info']->name]['video_length'] }} new)</h4>
                                <span>Added {{ $series['date'] }}</span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>

        <div class="home-list">
            <div class="home-list__title">
                <i class="fa fa-commenting-o home-list__icon" aria-hidden="true"></i>
                <div>
                    <h3 class="home-list__main-title-text">Pending - comments added</h3>
                    <span class="home-list__title-text">Files you have reviewed, on whitch you've added comments for rework. <br> you can view the screeners again, but can't add any more comments</span>
                </div>
            </div>

            <div class="home-list__items">
                @foreach($comments as $comment)
                    <a class="home-list__item" href="{{ route('video', [$comment->video_id]) }}">
                        <div class="home-list__left-item">
                            <div class="home-list__regular-item-text">
                                <h4>{{ strlen($comment->comment) > 150 ? substr($comment->comment, 0, 150) . '...' : $comment->comment }}</h4>
                            </div>
                        </div>

                        <div class="home-list__right-item">
                            <div class="home-list__regular-item-text">
                                <span>Comments submitted on {{ $comment->created_at }}</span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>

        <div class="home-list">
            <div class="home-list__title">
                <i class="fa fa-youtube-play home-list__icon" aria-hidden="true"></i>
                <div>
                    <h3 class="home-list__main-title-text">approved files</h3>
                    <span class="home-list__title-text">Files that doesn't need any rework, ready to be delivered for diffusion</span>
                </div>
            </div>

            <div class="home-list__items">
                @foreach($approved as $data)
                    <a class="home-list__item" href="{{ route('video', [$data->video->id]) }}">
                        <div class="home-list__left-item">
                            <div class="home-list__regular-item-text">
                                <h4>{{ $data->video->name }}</h4>
                            </div>
                        </div>

                        <div class="home-list__right-item">
                            <div class="home-list__regular-item-text">
                                <span>Approved on {{ $data->video->updated_at }}</span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection
