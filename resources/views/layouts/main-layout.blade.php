<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="header">
        <a class="header__logo" href="{{ url('/') }}">
            <img src="{{ asset('image/logo.png') }}" alt="bit" class="header__img">
        </a>

        <div class="logout">
            <a href="{{ route('logout') }}" class="logout__icon"
               onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                <i class="fa fa-sign-out" aria-hidden="true"></i>
            </a>
            <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </div>


    <div class="header-text">
        <span class="header-text__main-welcome">Welcome, Marie Sophie Lebranchet</span>

        <span class="header-text__welcome">Here are all the latest projects <br> you are currently working on.</span>

    </div>


    @yield('content')

</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
