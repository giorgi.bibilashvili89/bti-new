@extends('layouts.main-layout')

@section('content')
    <div class="main-container">

        <div class="home-list">

            <div class="home-list__items">
                @foreach($response['new'] as $video)
                    <a class="home-list__item" href="{{ route('video', [$video->id]) }}">
                        <div class="home-list__left-item">
                            <div class="home-list__item-icon">
                                <i class="fa fa-play-circle" aria-hidden="true"></i>
                            </div>

                            <div class="home-list__item-text">
                                <h4>{{ $video->series->name }}.{{ $video->name }}</h4>
                            </div>
                        </div>

                        <div class="home-list__right-item">
                            <div class="home-list__item-text home-list__item-text--uppercase">
                                <span class="home-list__item-text--bold">newly added</span>
                            </div>
                        </div>
                    </a>
                @endforeach

                @foreach($response['reviewed'] as $video)
                    <a class="home-list__item" href="{{ route('video', [$video->id]) }}">
                        <div class="home-list__left-item">
                            <div class="home-list__item-icon">
                                <i class="fa fa-play-circle" aria-hidden="true"></i>
                            </div>

                            <div class="home-list__item-text">
                                <h4>{{ $video->series->name }}.{{ $video->name }}</h4>
                            </div>
                        </div>

                        <div class="home-list__right-item">
                            <div class="home-list__item-text home-list__item-text--uppercase">
                                <span>review in progress</span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection
