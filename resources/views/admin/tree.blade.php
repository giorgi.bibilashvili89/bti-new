@extends('admin.layouts.main')

@section('content')
    <tree :tree="{{ json_encode($tree) }}" :teams="{{ json_encode($teams) }}" />
@endsection
