@extends('admin.layouts.login-layout')

@section('content')
    <div class="login">

        <form method="POST" action="{{ route('admin.login') }}">
            {{ csrf_field() }}

            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <span class="input__error input__error--center">{{ $error }}</span>
                @endforeach
            @endif

            <div class="input">
                <input type="text" class="input__text @if ($errors->has('email')) input__text--error @endif"
                       placeholder="email" name="email"/>
                @if ($errors->has('email'))
                    <span class="input__error">{{ $errors->first('email') }}</span>
                @endif
            </div>

            <div class="input">
                <input type="password" class="input__text @if ($errors->has('password')) input__text--error @endif"
                       placeholder="password" name="password"/>
                @if ($errors->has('password'))
                    <span class="input__error">{{ $errors->first('password') }}</span>
                @endif
            </div>

            <button type="submit" class="btn btn__primary btn__primary--uppercase">log in</button>
        </form>
    </div>
@endsection

<style scope>
    .login .input {
        margin-bottom: 1.5rem;
    }
</style>