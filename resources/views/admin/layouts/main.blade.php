<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="admin-header__menu">
        <div class="admin-header">
            <div class="admin-header__item">
                <ul>
                    <li class="admin-header__list {{ request()->is('admin') ? 'admin-header__list--active' : '' }}">
                        <a href="{{ route('admin.dashboard') }}">request</a>
                    </li>
                    <li class="admin-header__list {{ request()->is('admin/users') ? 'admin-header__list--active' : '' }}">
                        <a href="{{ route('admin.users') }}">users</a>
                    </li>
                    <li class="admin-header__list {{ request()->is('admin/tree') ? 'admin-header__list--active' : '' }}">
                        <a href="{{ route('admin.tree') }}">screeners</a>
                    </li>
                    <li class="admin-header__list">
                        <a href="#">activity</a>
                    </li>
                </ul>
            </div>

            <div class="admin-header__item">
                <div class="input">
                    <input type="text" class="input__text"/>
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>



    <div class="admin-container">
        @yield('content')
    </div>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
