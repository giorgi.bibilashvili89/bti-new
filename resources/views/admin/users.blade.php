@extends('admin.layouts.main')

@section('content')
    <users :users-data="{{ json_encode($users) }}"/>
@endsection
