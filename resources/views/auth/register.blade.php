@extends('layouts.login-layout')

@section('content')
    <div class="register">

        <h2 class="register__header-text">
            Please, fill <strong>all the fields below</strong> <br> to create your account
        </h2>

        <form class="register__form" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <div class="input">
                <input type="text" class="input__text @if ($errors->has('surname')) input__text--error @endif"
                       placeholder="surname*" name="surname" value="{{ old('surname') }}"/>
                @if ($errors->has('surname'))
                    <span class="input__error">{{ $errors->first('surname') }}</span>
                @endif
            </div>

            <div class="input">
                <input type="text" class="input__text @if ($errors->has('name')) input__text--error @endif"
                       placeholder="name*" name="name" value="{{ old('name') }}"/>
                @if ($errors->has('name'))
                    <span class="input__error">{{ $errors->first('name') }}</span>
                @endif
            </div>

            <div class="input">
                <input type="email" class="input__text @if ($errors->has('email')) input__text--error @endif"
                       placeholder="professional email address*" name="email" value="{{ old('email') }}"/>
                @if ($errors->has('email'))
                    <span class="input__error">{{ $errors->first('email') }}</span>
                @endif
            </div>

            <div class="input">
                <input type="text" class="input__text @if ($errors->has('mobile_number')) input__text--error @endif"
                       placeholder="mobile number*" name="mobile_number" value="{{ old('mobile_number') }}"/>
                @if ($errors->has('mobile_number'))
                    <span class="input__error">{{ $errors->first('mobile_number') }}</span>
                @endif
            </div>

            <div class="input">
                <input type="text" class="input__text @if ($errors->has('company_name')) input__text--error @endif"
                       placeholder="company's name*" name="company_name" value="{{ old('company_name') }}"/>
                @if ($errors->has('company_name'))
                    <span class="input__error">{{ $errors->first('company_name') }}</span>
                @endif
            </div>

            <div class="input">
                <input type="text" class="input__text @if ($errors->has('job_title')) input__text--error @endif"
                       placeholder="job's title*" name="job_title" value="{{ old('job_title') }}"/>
                @if ($errors->has('job_title'))
                    <span class="input__error">{{ $errors->first('job_title') }}</span>
                @endif
            </div>

            <div class="register__form__confirm">
                <div class="register__form__checkbox">
                    <label class="checkbox @if ($errors->has('job_title')) checkbox--error @endif">
                        <input type="checkbox" name="confirm" @if(old('confirm') == 'on') checked @endif>
                        <span class="checkmark"></span>
                    </label>
                </div>

                <div class="register__form__text">
                    <span>
                        I understand that the information I submited will not be used for any other purpose then creating an account, in order to access the Koder private interface.
                    </span>
                </div>
            </div>

            <div class="register__form__btn">
                <button type="submit" class="btn btn__primary btn__primary--uppercase">send</button>
            </div>
        </form>
    </div>
@endsection
