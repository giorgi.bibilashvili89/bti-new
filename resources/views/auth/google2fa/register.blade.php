@extends('layouts.login-layout')

@section('content')

    <div class="login google-2fa">

        <h2 class="login__header-text">Welcome to Koder, <br> your private work space.</h2>


        <div class="input">
            <label class="input__label">Log in</label>

            <input type="text" class="input__text" value="{{ $user->email }}" disabled/>
        </div>

        <span class="google-2fa__text">Scan this QR code with <strong>Google Authentication</strong> app to pair it with Koder</span>

        <img src="{{ $QR_Image }}">

        <span class="google-2fa__text">Enter the <strong>6-digital code</strong> generated</span>

        <form action="{{ route('google2fa') }}" method="GET">
            <div class="input">
                <input type="number" class="input__text @if ($errors->has('error')) input__text--error @endif"
                       name="one_time_password"/>
                @if ($errors->has('error'))
                    <span class="input__error">{{ $errors->first('error') }}</span>
                @endif
            </div>
        </form>

        <span class="google-2fa__text">Don't have the Google Authentication yet? <br> <a
                    href="https://www.google.com/landing/2step/" target="_blank">Download the app</a> on your smartphone</span>


    </div>
@endsection
