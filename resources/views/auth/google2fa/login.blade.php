@extends('layouts.login-layout')

@section('content')
    <div class="login">

        <h2 class="login__header-text">Welcome to Koder, <br> your private work space.</h2>
        {{ csrf_field() }}

        <div class="input">
            <label class="input__label">Log in</label>

            <input type="text" class="input__text" value="{{ Auth::user()->email }}" disabled/>
        </div>

        <span class="google-2fa__text">Enter the 6-digit code from your code generator (Google Authenticator)</span>

        <form method="POST" action="{{ route('2fa') }}">
            {{ csrf_field() }}

            <div class="input">
                <input type="number" class="input__text @if (Session::get('error') && !is_null(Session::get('error')['code'])) input__text--error @endif"
                       name="one_time_password" value="@if ($error = Session::get('error')){{ $error['code'] }}@endif"/>
                @if (Session::get('error') && !is_null(Session::get('error')['code']))
                    <span class="input__error">{{ Session::get('error')['message'] }}</span>
                @endif
            </div>
        </form>
    </div>
@endsection
