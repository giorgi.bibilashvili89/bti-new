@extends('layouts.login-layout')

@section('content')
    <div class="login">

        <h2 class="login__header-text">Welcome to Koder, <br> your private work space.</h2>
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            @if ($message = Session::get('error'))
                <div class="login_error">
                    <span class="input__error">{{ $message }}</span>
                </div>
            @endif

            <div class="input">
                <label class="input__label">Log in</label>

                <input type="text" class="input__text @if ($errors->has('email')) input__text--error @endif" placeholder="professional email address*" name="email" />
                @if ($errors->has('email'))
                    <span class="input__error">{{ $errors->first('email') }}</span>
                @endif
            </div>
        </form>

        <div class="login__footer">
            <span class="login__footer__text">First visit?</span>
            <a href="{{ route('register') }}" class="login__footer__link">Create an account</a>
        </div>
    </div>
@endsection
