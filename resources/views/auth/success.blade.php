@extends('layouts.login-layout')

@section('content')
    <div class="success-register">
        <span class="success-register__text">Your account request has been submitted to the administrators at BTI. They will validate it soon.</span>

        <span class="success-register__text">A confirmation email will be sent to your professional email.</span>

        <a href="{{ url('/') }}" class="btn btn__primary btn__link btn__primary--uppercase">Login</a>

    </div>
@endsection
