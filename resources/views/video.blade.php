@extends('layouts.main-layout')

@section('content')
    <video-page :video-tree="{{ json_encode($videoTree) }}"
                :default-comments="{{ json_encode($comments) }}"
                :team-users="{{ json_encode($teamUsers) }}"
                :video-info="{{ json_encode($videoInfo) }}"
                :user-data="{{ json_encode($userData) }}"
                :assets="{{ json_encode($assets) }}"
                video-folder-name="{{ env('SCAN_FOLDER_NAME', '/videos') }}"
    />
@endsection
