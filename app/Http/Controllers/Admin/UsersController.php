<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserPermission;
use App\Classes\JsonResponse;
use App\User;

// Request
use App\Http\Requests\Admin\Users\ChangeActiveStatusRequest;
use App\Http\Requests\Admin\Users\ChangePermissionRequest;
use App\Http\Requests\Admin\Users\DeleteUserRequest;
use App\Http\Requests\Admin\Users\EditUserRequest;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::whereDelete(0)->with(['permission', 'teams'])->get();

        return view('admin.users', compact('users'));
    }

    /**
     * Change Permission
     *
     * @param ChangePermissionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePermission(ChangePermissionRequest $request)
    {
        // Check If Request ID Correct
        if (!UserPermission::checkID($request->permission_id)) return JsonResponse::getErrorCode(1002);

        $update = UserPermission::updateOrCreate(
            ['id' => $request->permission_id],
            [$request->permission => $request->status]
        );

        // Check If Successfully Updated User Permission Status
        if (!$update) return JsonResponse::getErrorCode(1000);

        // Return Success Response
        return JsonResponse::success();
    }

    /**
     * Change Active Status
     *
     * @param ChangeActiveStatusRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeActiveStatus(ChangeActiveStatusRequest $request)
    {
        // Check If Request ID Correct
        if (!User::checkID($request->user_id)) return JsonResponse::getErrorCode(1002);

        $update = User::updateOrCreate(
            ['id' => $request->user_id],
            ['is_validate' => $request->status]
        );

        // Check If Successfully Updated User Permission Status
        if (!$update) return JsonResponse::getErrorCode(1000);

        // Return Success Response
        return JsonResponse::success();
    }

    /**
     * Delete User
     *
     * @param DeleteUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser(DeleteUserRequest $request)
    {
        // Check If Request ID Correct
        if (!User::checkID($request->user_id)) return JsonResponse::getErrorCode(1002);

        $update = User::updateOrCreate(
            ['id' => $request->user_id],
            ['delete' => 1]
        );

        // Check If Successfully Updated User Permission Status
        if (!$update) return JsonResponse::getErrorCode(1000);

        $users = User::whereDelete(0)->with(['permission', 'teams'])->get();

        // Return Success Response
        return JsonResponse::success($users);
    }

    /**
     * Edit User
     *
     * @param EditUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editUser(EditUserRequest $request)
    {
        // Check If Request ID Correct
        if (!User::checkID($request->user_id)) return JsonResponse::getErrorCode(1002);

        $update = User::updateOrCreate(
            ['id' => $request->user_id],
            [$request->input => $request->value]
        );

        // Check If Successfully Updated User Permission Status
        if (!$update) return JsonResponse::getErrorCode(1000);

        $users = User::whereDelete(0)->with(['permission', 'teams'])->get();

        // Return Success Response
        return JsonResponse::success($users);
    }
}
