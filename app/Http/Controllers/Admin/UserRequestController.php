<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserPermission;
use App\Classes\JsonResponse;
use App\Models\Request;
use App\User;

// Request
use App\Http\Requests\Admin\ChangeUserStatusRequest;

class UserRequestController extends Controller
{
    /**
     * User Request Page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // Get Not Confirm User Requests
        $getNotConfirmRequests = Request::getNotConfirmRequests()
            ->with('user')
            ->orderBy('id', 'DESC')
            ->get();
        // Return User Request Page With Not Confirm User Requests
        return view('admin.user-request', compact('getNotConfirmRequests'));
    }

    /**
     * Change User Status
     *
     * @param ChangeUserStatusRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeUserStatus(ChangeUserStatusRequest $request)
    {
        // Check If Request ID Correct
        if (!Request::checkID($request->request_id)) return JsonResponse::getErrorCode(1001);
        // Check User Status
        if ($request->status) {
            // Get User Info
            $getUserInfo = Request::find($request->request_id)->user;
            // Create User Permission
            $createPermission = UserPermission::create([
                'user_id' => $getUserInfo->id
            ]);
            // Update User Status
            $update = User::updateOrCreate(
                ['id' => $getUserInfo->id],
                [
                    'is_validate'   => $request->status,
                    'permission_id' => $createPermission->id
                ]
            );
            // Check If Successfully Updated User Request Status
            if (!$update) return JsonResponse::getErrorCode(1000);
        }
        // Update User Request Status
        $update = Request::updateOrCreate(
            ['id' => $request->request_id],
            ['request_status' => $request->status]
        );
        // Check If Successfully Updated User Request Status
        if (!$update) return JsonResponse::getErrorCode(1000);
        // Get Not Confirm User Requests
        $getNotConfirmRequests = Request::getNotConfirmRequests()
            ->with('user')
            ->orderBy('id', 'DESC')
            ->get();
        // Return Success Response
        return JsonResponse::success($getNotConfirmRequests);
    }
}
