<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Classes\JsonResponse;
use App\Models\TeamAccess;
use App\Models\TreeView;
use App\Models\Team;

// Request
use App\Http\Requests\Admin\VideoGroupRequest;

class TreeController extends Controller
{
    /**
     * Return Tree
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.tree', [
            'tree'  => $this->getTreeList(false),
            'teams' => Team::all()
        ]);
    }

    /**
     * Get Team Array
     *
     * @param $teams
     * @return array
     */
    protected function getTeamsArray($teams)
    {
        // Empty Team Array
        $teamArray = [];
        foreach ($teams as $team) {
            // Push Team ID
            array_push($teamArray, $team->id);
        }
        // Return Team Array
        return $teamArray;
    }

    /**
     * Get File Tree
     *
     * @param $file
     * @param $tree
     * @return mixed
     */
    private function getTree($file, $tree)
    {
        // Check If File Have children File
        if ($file->children) {
            // Loop Each Child File
            foreach ($file->children as $key => $file) {
                // Create Children Array
                $tree['children'][$key] = [
                    'id'    => $file->id,
                    'label' => $file->name,
                    'type'  => $file->type,
                    'video' => $file->video,
                    'teams' => $file->video ? $this->getTeamsArray($file->video->teams) : []
                ];
                // Check If Child File Have His Own Child File
                if (count($file->children)) $tree['children'][$key] = $this->getTree($file, $tree['children'][$key]);
            }
        }
        // Return Final Result
        return $tree;
    }

    /**
     * Get File Tree List
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTreeList($json = true)
    {
        // Get Files Tree
        $files = TreeView::with('video')->get()->toTree();
        // Empty Final Result
        $result = [];
        // Loop Each files
        foreach ($files as $key => $file) {
            // Create Parent
            $tree = [
                'id'    => $file->id,
                'label' => $file->name,
                'type'  => $file->type,
                'video' => $file->video,
                'teams' => $file->video ? $this->getTeamsArray($file->video->teams) : []
            ];
            // Push Permission Tree
            array_push($result, $this->getTree($file, $tree));
        }
        // Return Result
        return $json ? JsonResponse::success($result) : $result;
    }

    /**
     * Add/Delete Video Group
     *
     * @param VideoGroupRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function videoGroup(VideoGroupRequest $request)
    {
        // Get Video ID
        $videoID = TreeView::find($request->file_id)['video_id'];
        // Check Video ID
        if (!$videoID) return JsonResponse::getErrorCode(1000);
        // Check Team ID
        if (!Team::find($request->team_id)) return JsonResponse::getErrorCode(1000);
        // Check Status
        if ($request->status) {
            // Create Team Access
            TeamAccess::create([
                'video_id' => $videoID,
                'team_id'  => $request->team_id
            ]);
        } else {
            // Delete Team Access
            TeamAccess::whereVideoId($videoID)->whereTeamId($request->team_id)->delete();
        }
        // Return Success Response
        return JsonResponse::success();
    }
}
