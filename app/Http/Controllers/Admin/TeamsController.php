<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Classes\JsonResponse;
use App\Models\UserGroup;
use App\Models\Team;
use App\User;

// Requests
use App\Http\Requests\Admin\Teams\AddTeamRequest;
use App\Http\Requests\Admin\Teams\AddUserRequest;
use App\Http\Requests\Admin\Teams\DeleteUserRequest;
use App\Http\Requests\Admin\Teams\DeleteTeamRequest;
use App\Http\Requests\Admin\Teams\EditTeamRequest;


class TeamsController extends Controller
{
    /**
     * Get Team List
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTeamList()
    {
        return JsonResponse::success(Team::with('users')->get());
    }

    /**
     * Add User
     *
     * @param AddUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addUser(AddUserRequest $request)
    {
        // Check If Request ID Correct
        if (!Team::checkID($request->team_id)) return JsonResponse::getErrorCode(1003);

        if ($getUser = User::whereName($request->name)->first()) {

            if (UserGroup::whereUserId($getUser->id)->whereTeamId($request->team_id)->count())
                return JsonResponse::getErrorCode(1004);

            UserGroup::create([
                'user_id' => $getUser->id,
                'team_id' => $request->team_id
            ]);

            return $this->getTeamList();
        }

        return JsonResponse::getErrorCode(1002);
    }

    /**
     * Add Team
     *
     * @param AddTeamRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addTeam(AddTeamRequest $request)
    {
        if (Team::whereName($request->name)->count()) return JsonResponse::getErrorCode(1005);

        $create = Team::create([
            'name' => $request->name
        ]);

        // Check If Successfully Created
        if (!$create) return JsonResponse::getErrorCode(1000);

        return $this->getTeamList();
    }

    /**
     * Delete Team
     *
     * @param DeleteTeamRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTeam(DeleteTeamRequest $request)
    {
        // Check If Request ID Correct
        if (!Team::checkID($request->team_id)) return JsonResponse::getErrorCode(1003);

        $delete = Team::whereId($request->team_id)->delete();

        // Check If Successfully Deleted
        if (!$delete) return JsonResponse::getErrorCode(1000);

        return $this->getTeamList();
    }

    /**
     * Delete User
     *
     * @param DeleteUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function DeleteUser(DeleteUserRequest $request)
    {
        // Check If Request ID Correct
        if (!Team::checkID($request->team_id)) return JsonResponse::getErrorCode(1003);

        // Check If Request ID Correct
        if (!User::checkID($request->user_id)) return JsonResponse::getErrorCode(1002);

        $delete = UserGroup::whereUserId($request->user_id)->whereTeamId($request->team_id)->delete();

        // Check If Successfully Deleted
        if (!$delete) return JsonResponse::getErrorCode(1000);

        return $this->getTeamList();
    }

    /**
     * Edit Team Name
     *
     * @param EditTeamRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editTeam(EditTeamRequest $request)
    {
        // Check If Request ID Correct
        if (!Team::checkID($request->team_id)) return JsonResponse::getErrorCode(1003);

        $update = Team::updateOrCreate(
            ['id' => $request->team_id],
            ['name' => $request->value]
        );

        // Check If Successfully Updated
        if (!$update) return JsonResponse::getErrorCode(1000);

        return $this->getTeamList();
    }
}
