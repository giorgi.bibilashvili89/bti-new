<?php

namespace App\Http\Controllers;

use App\Classes\JsonResponse;
use Illuminate\Http\Request;
use App\Models\VideoComment;
use App\Models\TreeView;
use App\Models\Comment;
use App\Models\Assets;
use App\Models\Video;
use App\User;

use App\Http\Requests\Video\EditCommentRequest;
use App\Http\Requests\Video\AddCommentRequest;
use App\Http\Requests\IdRequest;

class VideoController extends Controller
{
    /**
     * Format Bytes
     *
     * @param $size
     * @param int $precision
     * @return string
     */
    public function formatBytes($size, $precision = 2)
    {
        $base = log($size, 1024);
        $suffixes = ['', 'KB', 'MB', 'GB', 'TB'];

        return round(pow(1024, $base - floor($base)), $precision) . '' . $suffixes[floor($base)];
    }

    /**
     * Show the Video Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        // Get Video Path
        $videoTree = TreeView::whereVideoId($id)->first();
        // Comments
        $comments = VideoComment::whereVideoId($id)
            ->with('comment')
            ->with('comment.user')
            ->whereTeamId(count(\Auth::user()->teams) ? \Auth::user()->teams[0]->id : 0)
            ->get();
        // Video Data
        $videoInfo = [
            'video_size' => $this->formatBytes(\File::size($_SERVER['DOCUMENT_ROOT'] .'/videos/' . $videoTree->path)),
            'video_data' => Video::find($id)
        ];
        // Get Teams
        $teamUsers = User::find(\Auth::user()->id)
            ->with('teams')
            ->with('permission')
            ->get();
        // User Data
        $userData = User::whereId(\Auth::user()->id)
            ->with('permission')
            ->first();
        // Assets
        $assets = Assets::whereVideoId($id)
            ->with('video')
            ->get();
        // Return Video Page Blade
        return view('video', compact('videoTree', 'comments', 'teamUsers', 'videoInfo', 'userData', 'assets'));
    }

    /**
     * Add Comment
     *
     * @param AddCommentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addComment(AddCommentRequest $request)
    {
        // Add Comment
        $comment = Comment::create([
            'video_id'   => $request->video_id,
            'user_id'    => \Auth::user()->id,
            'comment'    => $request->comment,
            'video_time' => $request->video_time
        ]);
        // Add Video Comment
        $videoComment = VideoComment::create([
            'video_id'   => $request->video_id,
            'team_id'    => count(\Auth::user()->teams) ? \Auth::user()->teams[0]->id : 0,
            'comment_id' => $comment->id
        ]);
        // Get Comments
        $comments = VideoComment::whereVideoId($request->video_id)
            ->with('comment')
            ->with('comment.user')
            ->whereTeamId(count(\Auth::user()->teams) ? \Auth::user()->teams[0]->id : 0)
            ->get();
        // Return Response
        return JsonResponse::success($comments);
    }

    /**
     * Delete Comment
     *
     * @param IdRequest $request
     * @return \Illuminate\Config\Repository|\Illuminate\Http\JsonResponse|mixed
     */
    public function deleteComment(IdRequest $request)
    {
        // Get Comment
        $comment = Comment::find($request->id);
        // Video ID
        $videoId = $comment->video_id;
        // Delete Comment
        $delete = $comment->delete();
        // Check If Successfully Deleted
        if (!$delete) return JsonResponse::errorCode(1000);
        // Get Comments
        $comments = VideoComment::whereVideoId($videoId)
            ->with('comment')
            ->with('comment.user')
            ->whereTeamId(count(\Auth::user()->teams) ? \Auth::user()->teams[0]->id : 0)
            ->get();
        // Return Response
        return JsonResponse::success($comments);
    }

    /**
     * Edit Comment
     *
     * @param EditCommentRequest $request
     * @return \Illuminate\Config\Repository|\Illuminate\Http\JsonResponse|mixed
     */
    public function editComment(EditCommentRequest $request)
    {
        $update = Comment::updateOrCreate(
            ['id' => $request->commentId],
            ['comment' => $request->comment]
        );
        // Check If Successfully Updated
        if (!$update) return JsonResponse::errorCode(1000);
        // Get Comments
        $comments = VideoComment::whereVideoId($update->video_id)
            ->with('comment')
            ->with('comment.user')
            ->whereTeamId(count(\Auth::user()->teams) ? \Auth::user()->teams[0]->id : 0)
            ->get();
        // Return Response
        return JsonResponse::success($comments);
    }

    /**
     * Approvo
     *
     * @param IdRequest $request
     * @return \Illuminate\Config\Repository|\Illuminate\Http\JsonResponse|mixed
     */
    public function approvo(IdRequest $request)
    {
        $update = Video::updateOrCreate(
            ['id' => $request->id],
            ['status' => 1]
        );
        // Check If Successfully Updated
        if (!$update) return JsonResponse::errorCode(1000);
        // Return Response
        return JsonResponse::success();
    }
}