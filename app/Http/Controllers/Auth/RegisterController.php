<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Request as UserRequest;
use App\User;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/success/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email'         => 'required|string|email|max:255|unique:users',
            'surname'       => 'required|string|max:255',
            'name'          => 'required|string|max:255',
            'mobile_number' => 'required|string|max:20',
            'company_name'  => 'required|string|max:255',
            'job_title'     => 'required|string|max:255',
            'confirm'       => 'accepted'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // Create User
        $user = User::create([
            'email'         => $data['email'],
            'surname'       => $data['surname'],
            'name'          => $data['name'],
            'mobile_number' => $data['mobile_number'],
            'company_name'  => $data['company_name'],
            'job_title'     => $data['job_title'],
            'password'      => bcrypt('1'), // We Just Add Default Password For Every User
            'is_validate'   => 0
        ]);
        // Create User Request
        UserRequest::create(['user_id' => $user->id]);
        // Return User
        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    public function showSuccessRegister()
    {
        return view('auth.success');
    }
}
