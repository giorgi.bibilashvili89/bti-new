<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Series;

class SeriesController extends Controller
{
    /**
     * Show the Series List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $response = Series::getVideosFromTeams(\Auth::user(), $request, $id);

        return view('series', compact('response', 'request', 'id'));
    }
}
