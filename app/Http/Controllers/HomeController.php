<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Series;
use App\Models\Comment;
use App\Models\TeamAccess;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = Series::getSeriesFromTeams(\Auth::user(), $request);

        $comments = Comment::whereUserId(\Auth::user()->id)
            ->orderBy('created_at', 'DESC')
            ->take(10)
            ->get();

        $approved = TeamAccess::with('video')
            ->whereTeamId(count(\Auth::user()->teams) ? \Auth::user()->teams[0]->id : 0)
            ->whereHas('video', function ($query) {
                $query->whereStatus(1);
            })
            ->get();

        return view('dashboard', compact('response', 'comments', 'request', 'approved'));
    }
}
