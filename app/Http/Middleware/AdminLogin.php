<?php

namespace App\Http\Middleware;

use Closure;

class AdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check If Admin User Is Authorized & Check If User Want To Access Admin Login Page
        if (\Auth::guard('admin')->check() && \Route::current()->getName() == 'admin.show.login') {
            // Redirect To Admin Dashboard
            return redirect()->route('admin.dashboard');
        }
        // Pass Request
        return $next($request);
    }
}
