<?php

namespace App\Http\Middleware;

use PragmaRX\Google2FALaravel\Support\Authenticator;
use Closure;

class Google2fa
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authenticator = app(Authenticator::class)->boot($request);

        if ($authenticator->isAuthenticated()) {
            return $next($request);
        }

        \Session::flash('error', [
            'code'    => $request->one_time_password,
            'message' => 'This code is not valid'
        ]);

        return $authenticator->makeRequestOneTimePasswordResponse();
    }
}
