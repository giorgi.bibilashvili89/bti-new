<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check If Admin User Is Authorized
        if (!\Auth::guard('admin')->check()) return redirect()->route('admin.show.login');
        // Pass Request
        return $next($request);
    }
}
