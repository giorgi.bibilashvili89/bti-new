<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserPermission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'view', 'chat', 'publish'];

    // Relationship

    /**
     * User Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    // Scope


    public function scopeCheckID($query, $id)
    {
        return $query->whereId($id)->exists();
    }
}
