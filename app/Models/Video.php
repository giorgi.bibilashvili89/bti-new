<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'series_id', 'name', 'type', 'status'];

    // Relationship

    /**
     * Videos That Belong To The Teams
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teams()
    {
        return $this->belongsToMany(Team::class, 'team_access');
    }

    /**
     * Video Relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function series()
    {
        return $this->belongsTo(Series::class, 'series_id');
    }

    /**
     * Videos That Belong To The Comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function comment()
    {
        return $this->belongsToMany(Comment::class, 'video_comments');
    }

    // scope

    /**
     * Get Series
     *
     * @param $query
     * @param $name
     * @return mixed
     */
    public function scopeGetVideo($query, $name)
    {
        return $query->whereName($name);
    }
}
