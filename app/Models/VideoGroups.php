<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoGroups extends Model
{
    /**
     * Table Name
     *
     * @var string
     */
    protected $table = 'video_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['video_id', 'series_id'];
}
