<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Video;

class Series extends Model
{
    /**
     * Table Name
     *
     * @var string
     */
    public $table = 'series';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name'];

    // Relationships

    /**
     * Series That Belong To The Videos
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function videos()
    {
        return $this->belongsToMany(Video::class, 'video_groups');
    }

    // Scopes

    /**
     * Get Series
     *
     * @param $query
     * @param $name
     * @return mixed
     */
    public function scopeGetSeries($query, $name)
    {
        return $query->whereName($name);
    }
}
