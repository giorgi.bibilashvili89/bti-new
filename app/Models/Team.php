<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Video;

class Team extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name'];

    // Relationship

    /**
     * Teams That Belong To The Users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_group');
    }

    /**
     * Teams That Belong To The Video
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function videos()
    {
        return $this->belongsToMany(Video::class, 'team_access');
    }

    /**
     * Videos That Belong To The Comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function comment()
    {
        return $this->belongsToMany(Comment::class, 'video_comments');
    }

    // Scope

    public function scopeCheckID($query, $id)
    {
        return $query->whereId($id)->exists();
    }
}
