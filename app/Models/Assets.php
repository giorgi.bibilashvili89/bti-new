<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Video;

class Assets extends Model
{
    public $table = 'assets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'path', 'video_id'];

    // Relationship

    /**
     * Teams That Belong To The Video
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function video()
    {
        return $this->belongsTo(Video::class, 'video_id');
    }
}
