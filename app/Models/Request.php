<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Request extends Model
{
    public $table = 'user_requests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'request_status'];

    // Relationship

    /**
     * User Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    // Scopes

    /**
     * Get Not Confirm Requests
     *
     * @param $query
     * @return mixed
     */
    public function scopeGetNotConfirmRequests($query)
    {
        return $query->whereRequestStatus(null);
    }

    public function scopeCheckID($query, $id)
    {
        return $query->whereId($id)->exists();
    }
}
