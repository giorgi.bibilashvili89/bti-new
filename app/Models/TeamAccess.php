<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamAccess extends Model
{
    /**
     * Table Name
     *
     * @var string
     */
    public $table = 'team_access';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['team_id', 'video_id'];

    // Relationship

    /**
     * Video Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function video()
    {
        return $this->belongsTo(Video::class, 'video_id');
    }
}
