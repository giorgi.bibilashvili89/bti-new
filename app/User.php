<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\UserPermission;
use App\Models\Team;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'surname', 'name', 'mobile_number', 'company_name', 'job_title', 'google2fa_secret', 'ip_address', 'is_validate', 'delete', 'permission_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['remember_token', 'google2fa_secret'];

    // Relationship

    public function permission()
    {
        return $this->belongsTo(UserPermission::class, 'permission_id');
    }

    /**
     * The Users That Belong To The Team
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teams()
    {
        return $this->belongsToMany(Team::class, 'user_group');
    }

    // Scope

    /**
     * Check Email
     *
     * @param $query
     * @param string $email
     * @param int $result // 0 = Just Check, 1 = Check And Return User
     * @return bool
     */
    public function scopeCheckEmail($query, string $email, int $result = 0)
    {
        // Search Email
        $query->whereEmail($email);
        // Check If Email Exit
        if ($query->count()) {
            // Return Result
            return $result ? $query->first() : true;
        }
        // Return Default Result
        return false;
    }

    public function scopeCheckID($query, $id)
    {
        return $query->whereId($id)->exists();
    }
}
