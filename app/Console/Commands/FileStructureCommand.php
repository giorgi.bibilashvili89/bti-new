<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\VideoGroups;
use App\Models\TreeView;
use App\Models\Series;
Use App\Models\Video;

class FileStructureCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'file:structure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get File Structure';

    /**
     * Folder Name
     *
     * @var string
     */
    protected $folderName = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Check If File is Really File
     *
     * @param $fileName
     * @return bool
     */
    protected function checkFile($fileName)
    {
        return strpos($fileName, ".") !== false;
    }

    /**
     * Save Series
     *
     * @param $file
     * @return mixed
     */
    protected function saveSeries($file)
    {
        // Split File Name
        $splitFileName = explode('.', $file);
        // Get Or Create NEw Series
        $series = Series::firstOrCreate(['name' => $splitFileName[0]]);
        // Create Video
        $video = Video::firstOrCreate([
            'series_id' => $series->id,
            'name'      => $splitFileName[1],
            'type'      => $splitFileName[2]
        ]);
        // Videos Groups
        VideoGroups::create([
            'video_id'  => $video->id,
            'series_id' => $series->id
        ]);
        // Return Video ID
        return $video->id;
    }

    /**
     * Get Folder Structure
     *
     * @param $folderPath
     * @param $parent
     */
    protected function folderStructure($folderPath, $parent)
    {
        // Loop Each File
        foreach (scandir($folderPath) as $file) {
            // Check If File Name Not Equal Dots
            if ($file == '.' || $file == '..') continue;
            // Check If File is Really File
            if ($this->checkFile($file)) {
                // Create Tree
                $createTree = TreeView::create([
                    'name'     => $file,
                    'path'     => $this->folderName . '/' . $file,
                    'type'     => 0,
                    'video_id' => $this->saveSeries($file)
                ]);
                // Add Parent
                if (!is_null($parent)) $createTree->appendToNode($parent)->save();
            } else {
                // Folder Name
                $this->folderName = $file;
                // Create Tree
                $createTree = TreeView::create([
                    'name' => $file,
                    'type' => 1
                ]);
                // Add Parent
                if (!is_null($parent)) $createTree->appendToNode($parent)->save();
                // Get Folder Structure
                $this->folderStructure($folderPath . '/' . $file, $createTree);
            }
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Delete All Rows In tree_views Table
        TreeView::truncate();
        // Delete All Rows In Video Groups Table
        VideoGroups::truncate();
        // Get Folder Structure
        $this->folderStructure(env('SCAN_FOLDER_NAME', './public/videos'), null);
    }
}
