<?php

namespace App\Classes;

use App\Models\Comment;

class Series
{
    public static function getSeriesFromTeams($user, $request)
    {
        $series = [
            'new'      => [],
            'reviewed' => []
        ];

        foreach ($user->teams as $team) {
            foreach ($team->videos as $video) {

                if ($video->status) continue;

                if (!is_null($request->search) && stripos($video->series->name, $request->search) === false) continue;

                if (Comment::getComment($user->id, $video->id)->count()) {
                    $lastComment = Comment::getComment($user->id, $video->id)->orderBy('created_at', 'DESC')->first();
                    $date = $lastComment->created_at;
                    $row = 'reviewed';
                } else {
                    $date = $video->created_at;
                    $row = 'new';
                }

                if (array_key_exists($video->series->name, $series[$row])) {
                    $series[$row][$video->series->name] = [
                        'video_length' => ++$series[$row][$video->series->name]['video_length'],
                        'info'         => $video->series,
                        'date'         => $date
                    ];
                } else {
                    $series[$row][$video->series->name] = [
                        'video_length' => 1,
                        'info'         => $video->series,
                        'date'         => $date
                    ];
                }
            }
        }

        $new = $series['new'];
        $reviewed = $series['reviewed'];

        if ($request->sort == 1) {
            krsort($new);
            krsort($reviewed);
        } elseif ($request->sort == 0) {
            ksort($new);
            ksort($reviewed);
        }

        return [
            'new'      => $new,
            'reviewed' => $reviewed
        ];
    }

    public static function getVideosFromTeams($user, $request, $id)
    {
        $series = [
            'new'      => [],
            'reviewed' => []
        ];

        foreach ($user->teams as $team) {
            foreach ($team->videos as $video) {
                if ($video->series->id != $id) continue;

                if ($video->status) continue;

                if (Comment::getComment($user->id, $video->id)->count()) {
                    $row = 'reviewed';
                } else {
                    $row = 'new';
                }

                $series[$row][$video->name] = $video;
            }
        }

        $new = $series['new'];
        $reviewed = $series['reviewed'];

        if ($request->sort == 1) {
            krsort($new);
            krsort($reviewed);
        } elseif ($request->sort == 0) {
            ksort($new);
            ksort($reviewed);
        }

        return [
            'new'      => $new,
            'reviewed' => $reviewed
        ];
    }
}