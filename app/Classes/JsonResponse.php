<?php

namespace App\Classes;

class JsonResponse
{
    /**
     * Error Code
     *
     * @param $code
     * @return \Illuminate\Config\Repository|mixed
     */
    static function errorCode($code)
    {
        return config('errors.' . $code);
    }

    /**
     * Get Error Code
     *
     * @param $code
     * @param null $attemptID
     * @param null $status
     * @param null $message
     * @return \Illuminate\Http\JsonResponse
     */
    static function getErrorCode($code, $attemptID = null, $status = null, $message = null)
    {
        // Check If This Error Code Exist
        if (!is_null(static::errorCode($code))) {
            return response()->json([
                'status'     => is_null($status) ?static::errorCode($code)['status'] : $status,
                'error_code' => $code,
                'message'    => is_null($message)
                    ? static::errorCode($code)['message']
                    : $message
            ])->setStatusCode(500, is_null($message) ? static::errorCode($code)['message'] : $message);
        }
        // Return Default Error Response
        return response()->json('Undefined Error code')->setStatusCode(500, 'Undefined Error code');
    }
    /**
     * Success Message
     *
     * @param null $data
     * @param null $status
     * @return \Illuminate\Http\JsonResponse
     */
    static function success($data = null, $status = null)
    {
        // Create Response Array
        $response = [
            'status' => is_null($status) ? true : $status,
        ];
        // Check If Response Is Null
        if (!is_null($data)) $response['data'] = $data;
        // Return Response
        return response()->json($response);
    }
}